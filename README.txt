CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration


INTRODUCTION
------------

Current Maintainer: Jyotisankar Pradhan <jyotisankar.pradhan@gmail.com>

Being an Administrator if you want to restrict user belongs to a specific roles
to login to your application in a given time frame this is the suitable module.
You can assign time slots for the users to login to the application, other than
the given time slot users can not log in to the application.

This module helps high traffic website to restrict users logging into it.

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
Administrator can set the time slots for roles from the admin panel. 
For adding slots navigate to 
<yourdomain.com>/admin/timelogin/timelogin_add_timeslot
 
To see the listing of the time slots added, from where you can edit/delete the
available slots navigate to the url given below or you can navigate thorough
the admin menu as well, the name of the menu is "Timelogin".
<yourdomain.com>/admin/timelogin
